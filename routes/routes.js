var appRouter = function (app, db, path) {
    /**
     * @api {get} /users/ Request all Users
     * @apiVersion 0.0.1
     *
     * @apiName GetUsers
     * @apiGroup User
     *
     * @apiSuccess {String} name Name of the User.
     * @apiSuccess {Integer} reps  Reps of the User.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *     "data":
     *      {
     *       "name": "John",
     *       "reps": 10
     *      }
     *     }
     *
     * @apiError UserNotFound No Users was not found.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "error": "UserNotFound"
     *     }
     */
    app.get('/users/', function (req, res) {
        db.all("SELECT * FROM repbot", function (err, row) {
            if (row.length > 0) {
                res.status(200);
                res.json({"data": row});
            } else {
                console.log(err);
                res.status(404);
                res.json({"error": "UserNotFound"});
            }
        });
    });

    /**
     * @api {post} /users/ Add User to database
     * @apiVersion 0.0.1
     *
     * @apiName AddUser
     * @apiGroup User
     *
     * @apiSuccess UserAdded User was added to database.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "success": "UserAdded"
     *     }
     *
     * @apiError UserNotAdded Users was not added.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *       "error": "UserNotAdded"
     *     }
     */
    app.post('/users/', function (req, res) {

        var name = req.body.name;
        var reps = req.body.reps;

        db.run("INSERT INTO repbot (name, reps) VALUES ('" + name + "', " + reps + ")", function (err, row) {
            if (err) {
                res.json({"error": "UserNotAdded"});
                res.status(500);
            } else {
                res.json({"success": "UserAdded"});
                res.status(200);
            }
            res.end();
        });
    });

    /**
     * @api {put} /users/ Update existing User
     * @apiVersion 0.0.1
     *
     * @apiName UpdateUser
     * @apiGroup User
     *
     * @apiSuccess UserUpdated User was updated.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "success": "UserUpdated"
     *     }
     *
     * @apiError UserNotUpdated Users was not updated.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *       "error": "UserNotUpdated"
     *     }
     */
    app.put('/users/', function (req, res) {

        var name = req.body.name;
        var reps = req.body.reps;

        db.run("UPDATE repbot SET reps = " + reps + " WHERE name = '" + name + "'", function (err, row) {
            if (err) {
                res.status(500);
                res.json({"error": "UserNotUpdated"});
            } else {
                res.status(200);
                res.json({"success": "UserUpdated"});
            }
            res.end();
        });
    });

    /**
     * @api {delete} /users/:user Delete existing User
     * @apiVersion 0.0.1
     *
     * @apiName DeleteUser
     * @apiGroup User
     *
     * @apiParam {String} user Users unique name.
     *
     * @apiSuccess UserDeleted User was deleted.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "success": "UserDeleted"
     *     }
     *
     * @apiError UserNotDeleted Users was not deleted.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *       "error": "UserNotDeleted"
     *     }
     */
    app.delete('/users/:user', function (req, res) {

        var name = req.params.user;

        db.run("DELETE FROM repbot WHERE name = '" + name + "'", function (err, row) {
            if (err) {
                res.status(500);
                res.json({"error": "UserNotDeleted"});
            } else {
                res.status(200);
                res.json({"success": "UserDeleted"});
            }
            res.end();
        });
    });

    /**
     * @api {get} /users/:user Request User information
     * @apiVersion 0.0.1
     *
     * @apiName GetUser
     * @apiGroup User
     *
     * @apiParam {String} user Users unique name.
     *
     * @apiSuccess {String} name Name of the User.
     * @apiSuccess {Integer} reps  Reps of the User.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *     "data":
     *      {
     *       "name": "John",
     *       "reps": 10
     *      }
     *     }
     *
     * @apiError UserNotFound No Users was not found.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "error": "UserNotFound"
     *     }
     */
    app.get('/users/:user', function (req, res) {

        var name = req.params.user;

        db.all("SELECT * FROM repbot WHERE name = '" + name + "' LIMIT 1", function (err, row) {
            if (row.length > 0) {
                res.status(200);
                res.json({"data": row});
            } else {
                res.status(404);
                res.json({"error": "UserNotFound"});
            }
        });
    });

    /**
     * @api {get} /repPool/ Request daily repPool amount
     * @apiVersion 0.0.1
     *
     * @apiName GetRepPool
     * @apiGroup Reps
     *
     * @apiSuccess {Integer} reps  The amount of reps left over for today.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *     "data":
     *      {
     *       "reps": 10
     *      }
     *     }
     */
    app.get('/repPool/', function (req, res) {
        db.all("SELECT * FROM dailyRepPool LIMIT 1", function (err, row) {
            if (row.length > 0) {
                res.status(200);
                res.json({"data": row});
            } else {
                res.status(404);
                res.json({"error": "RepsNotFound"});
            }
        });
    });

    /**
     * @api {post} /repPool/ Add daily repPool to database
     * @apiVersion 0.0.1
     *
     * @apiName AddRepPool
     * @apiGroup Reps
     *
     * @apiSuccess RepPoolAdded Daily repPool was added to database.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "success": "repPoolAdded"
     *     }
     *
     * @apiError RepPoolNotAdded Daily repPool was not added.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *       "error": "RepPoolNotAdded"
     *     }
     */
    app.post('/repPool/', function (req, res) {

        var reps = req.body.reps;

        db.run("INSERT INTO dailyRepPool (reps) VALUES (" + reps + ")", function (err, row) {
            if (err) {
                res.json({"error": "RepPoolNotAdded"});
                res.status(500);
            } else {
                res.json({"success": "repPoolAdded"});
                res.status(200);
            }
            res.end();
        });
    });

    /**
     * @api {put} /repPool/ Update daily repPool amount
     * @apiVersion 0.0.1
     *
     * @apiName UpdateRepPool
     * @apiGroup Reps
     *
     * @apiSuccess DailyPoolUpdated Daily Rep Pool was updated.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "success": "DailyPoolUpdated"
     *     }
     *
     * @apiError DailyPoolNotUpdated Daily Rep Pool was not updated.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *       "error": "DailyPoolNotUpdated"
     *     }
     */
    app.put('/repPool/', function (req, res) {

        var id = req.body.id;
        var reps = req.body.reps;

        db.run("UPDATE dailyRepPool SET reps = " + reps + ", timestamp = datetime('now') WHERE id = '" + id + "'", function (err, row) {
            if (err) {
                res.status(500);
                res.json({"error": "DailyPoolNotUpdated"});
            } else {
                res.status(200);
                res.json({"success": "DailyPoolUpdated"});
            }
            res.end();
        });
    });

    /**
     * @api {get} / Receive API Docs.
     * @apiVersion 0.0.1
     *
     * @apiName GetDocs
     * @apiGroup Docs
     *
     */
    app.get('/', function (req, res) {
        res.sendFile(path.join(__dirname + '/../apidoc/index.html'));
    });
}

module.exports = appRouter;