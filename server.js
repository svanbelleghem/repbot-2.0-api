var sqlite3 = require('sqlite3').verbose();
var schedule = require('node-schedule');

const path = require('path');
const dbPath = path.resolve(__dirname, 'repbot.db');
const db = new sqlite3.Database(dbPath);

var bodyParser = require('body-parser');
var express = require('express'),
    app = express();

app.use(bodyParser.json());

app.use(express.static(__dirname + '/apidoc/'));
app.use(express.static(__dirname + '/apidoc/css'));
app.use(express.static(__dirname + '/apidoc/fonts'));
app.use(express.static(__dirname + '/apidoc/img'));
app.use(express.static(__dirname + '/apidoc/locales'));
app.use(express.static(__dirname + '/apidoc/utils'));
app.use(express.static(__dirname + '/apidoc/vendor'));

app.use(bodyParser.urlencoded({ extended: true }));

// Setup
db.serialize(function () {
    db.run("CREATE TABLE IF NOT EXISTS repbot (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, reps INTEGER)");
    db.run("CREATE TABLE IF NOT EXISTS dailyRepPool (id INTEGER PRIMARY KEY AUTOINCREMENT, reps INTEGER NOT NULL DEFAULT 10, timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP)");
});

var routes = require("./routes/routes.js")(app, db, path);

// Reset Daily Pool every day at 23:59:59
var j = schedule.scheduleJob('59 59 23 * *', function(){
    // Request Daily Pool from database to be able to update this.
    request('http://localhost:3000/repPool/', function (error, response, body) {
        if (!error && response.statusCode == 200) {
            jsonData = JSON.parse(body);

            for (var i = 0; i < jsonData.data.length; i++) {
                var data = jsonData.data[i];
                var id = data.id;
                var reps = data.reps;
            }

            request.put('http://localhost:3000/repPool/').form({ "id": id, "reps": 10 });

        } else if (!error && response.statusCode == 404) {
            // Daily Pool doesn't exist yet.
            request.post('http://localhost:3000/repPool/').form({ "reps": 10 });
        }
    });
});

var server = app.listen(3000, function() {
    console.log("RepBot API server started on: ", server.address().port)
});